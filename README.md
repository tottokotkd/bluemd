# bluemd

ssパーサー

## input

```markdown
###裁判

@事務所 +比奈 +千枝
春菜　この中に…
メガネをかけていない人がいます！

千枝　わー

比奈　わー
```

## output

```json
{
   "title": "裁判",
   "level": 3,
   "contents": [
      {
         "type": "scene",
         "value": "@事務所 +比奈 +千枝"
      },
      {
         "type": "text",
         "value": "春菜　この中に…\nメガネをかけていない人がいます！"
      },
      {
         "type": "text",
         "value": "千枝　わー"
      },
      {
         "type": "text",
         "value": "比奈　わー"
      }
   ]
}
```
