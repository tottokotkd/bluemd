/**
 * Created by tottokotkd on 17/06/05.
 */

import {SceneRegister, SceneRegisterMode} from "./scene";

export class BackgroundRegister implements SceneRegister {
  public name: string;
  public mode: SceneRegisterMode;
  constructor(name: string, mode: SceneRegisterMode) {
    this.name = name;
    this.mode = mode;
  }
}

export default BackgroundRegister;
