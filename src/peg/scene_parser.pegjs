/*
 * Scene Parser
 * ==========================

* input: string
* output: object

## example 1

```
@事務所 + => 比奈 +春菜
```

[
  {mode: "add", type: "background", name: "事務所"},
  {mode: "add", type: "person", name: "比奈", position: "right", desc: []},
  {mode: "add", type: "person", name: "春菜", position: "auto", desc: []},
]

## example 2

```
+比奈 #照れ -春菜
```

[
  {mode: "add", type: "person", name: "比奈", desc: ["照れ"]},
  {mode: "remove", type: "person", name: "春菜"},
]

*/


Text
  = (Background / Person)*

Background
  = mode:BackgroundMode name:SceneString _ { return {type: "background", mode, name} }

BackgroundMode
  = '@' { return "add" }

Person
  = mode:PersonMode _ position:PersonPosition _ name:SceneString _ desc:Desc* { return {type: "person", mode, name, position, desc} }

PersonMode
  = '+' { return "add" }
  / '-' { return "remove" }

PersonPosition
  = ('<=' / "<-") { return "left" }
  / ('=>' / "->") { return "right" }
  / ''{ return "auto" }

Desc
  = '#' desc:SceneString _ { return desc }

SceneString
  = str:EscapedValue
  / str:[^\r\n#@+-　 ]+ { return str.join('') }

NewLine
  = [\r?\n]

_
  = Space*

Space
  = [　 ]

// https://stackoverflow.com/questions/33947960/allowing-for-quotes-and-unicode-in-peg-js-grammar-definitions
EscapedValue
  = '"' chars:DoubleStringCharacter* '"' { return chars.join(''); }
  / "'" chars:SingleStringCharacter* "'" { return chars.join(''); }

DoubleStringCharacter
  = !('"' / "\\") char:. { return char; }
  / "\\" sequence:EscapeSequence { return sequence; }

SingleStringCharacter
  = !("'" / "\\") char:. { return char; }
  / "\\" sequence:EscapeSequence { return sequence; }

EscapeSequence
  = "'"
  / '"'
  / "\\"
  / "b"  { return "\b";   }
  / "f"  { return "\f";   }
  / "n"  { return "\n";   }
  / "r"  { return "\r";   }
  / "t"  { return "\t";   }
  / "v"  { return "\x0B"; }
