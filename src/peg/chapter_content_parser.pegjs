/*
 * Chapter Content Parser
 * ==========================

* input: string
* output: object

```
###裁判

@事務所 +比奈 +千枝
春菜　この中に…
メガネをかけていない人がいます！

千枝　わー

比奈　わー
```

{
   "title": "裁判",
   "level": 3,
   "contents": [
      {
         "type": "scene",
         "value": "@事務所 +比奈 +千枝"
      },
      {
         "type": "text",
         "value": "春菜　この中に…
メガネをかけていない人がいます！"
      },
      {
         "type": "text",
         "value": "千枝　わー"
      },
      {
         "type": "text",
         "value": "比奈　わー"
      }
   ]
}

*/

Chapter
  = t:Title __ contents:ChapterContent { return {title: t.title, level: t.level, contents} }
  / __ contents:ChapterContent { return {title: undefined, level: undefined, contents} }

Title
  = level:ChapterHeader title:SingleLineValue { return {level, title} }
  / level:ChapterHeader { return {level} }

ChapterContent
  = e:ChapterElement __ es:ChapterContent { return [e].concat(es) }
  / e:ChapterElement { return [e] }

ChapterLine
  = line:(!Title .)*  { return line.map(t => t[1]).join('') }

ChapterElement
  = (Scene / Text)

Scene
  = prefix:[:@+\-] vs:SingleLineValue { return {type: "scene", value: prefix + vs} }

Text
  = v:SingleLineValue NewLine vs:Text { return {type: "text", value: v + "\n" + vs.value} }
  / v:SingleLineValue NewLine? { return {type: "text", value: v}  }

TextEnd
  = NewLine NewLine

ChapterHeader
  = h:'#'+ _ { return h.length }

SingleLineValue
  = v:(! NewLine .) vs:SingleLineValue { return v[1] + vs }
  / v:(! NewLine .) { return v[1] }

NewLine
  = [\r?\n]

__
  = (Space / NewLine)*

_
  = Space*

Space
  = [　 ]
