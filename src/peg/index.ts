/**
 * Created by tottokotkd on 17/06/03.
 */

import {parse as parseToChapterContent} from "./chapter_content_parser.pegjs";
import {parse as parseToChapters} from "./chapters_parser.pegjs";
import {parse as parseToComment} from "./comment_parser.pegjs";
import {parse as parseToScene} from "./scene_parser.pegjs";
import {parse as parseToText} from "./text_parser.pegjs";

export interface IChapter {
  title?: string;
  level?: number;
  contents: IChapterContent[];
}

export interface IChapterContent {
  type: ChapterContentType;
  value: string;
}

export type ChapterContentType = "scene" | "text";

export interface ISpeaker {
  name: string;
  desc: string[];
}

export interface IText {
  title: string;
  speaker?: ISpeaker;
  values: string[];
}

export type SceneRegisterMode = "add" | "remove" | "manual" | "auto";
export type SceneResourceType = "person" | "background";
export interface ISceneRegister {
  mode: SceneRegisterMode;
  type: SceneResourceType;
  name: string;
}

export type PersonPosition = "left" | "right" | "auto";
export interface IPersonRegister extends ISceneRegister {
  position?: string;
  desc: string[];
}

export function parseChapters(input: string): string[] {
  return parseToChapters(input);
}

export function parseChapterContent(input: string): IChapter {
  return parseToChapterContent(input);
}

export function parseComment(input: string): string {
  return parseToComment(input);
}

export function parseText(input: string): IText {
  return parseToText(input.trim());
}

export function parseScene(input: string): ISceneRegister[] {
  return parseToScene(input.trim());
}
