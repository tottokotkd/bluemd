/*
 * Comment Parser
 * ==========================

* input: string
* output: string

```
春菜　メガネ<!--

 !!! 漢字にするか考える !!!

-->ですよ！

<!--

ここではシーン設定ができる

@事務所 +比奈 #困惑

タイトルも設定できる

:猫おじさん

-->

比奈　うーん… <!--
@事務所 ここには書けない
-->
```

"春菜　メガネですよ！

@事務所 +比奈 #困惑
:猫おじさん

比奈　うーん… "

*/

Data = i:Input { return i.reduce((result, a) => result.concat(a), []).join("\n") }

Input
  = l:Line NewLine ls:Input { return [l].concat(ls) }
  / l:Line { return [l] }

Line
  = c:ParsableComment {return c }
  / content:LineContent*  { return [content.filter(c => c !== null).join('')] }

LineContent
  = c:(!CommentBegin !NewLine .) { return c[2] }
  / Comment

Comment = CommentBegin (!CommentEnd .)* CommentEnd { return null }
CommentContent = (!CommentEnd .)*

ParsableComment = CommentBegin c:ParsableCommentContent CommentEnd { return c.filter(c => c !== null) }
ParsableCommentContent
  = c:ParsableCommentValue NewLine cs:ParsableCommentContent {return [c].concat(cs) }
  / c:ParsableCommentValue { return [c] }

ParsableCommentValue
  = _ prefix:(!CommentEnd [@+:\-]) values:(!CommentEnd !NewLine .)* { return prefix[1] + values.map(v => v[2]).join('') }
  / values:(!CommentEnd !NewLine .)* { return null }

CommentBegin = '<!--'
CommentEnd = '-->'

_
  = Space*

NewLine
  = [\r?\n]

Space
  = [　 ]
