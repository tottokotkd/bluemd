/*
 * Chapter Parser
 * ==========================

* input: string
* output: string[]

```

Content

### title1
Content A

Content B

#title2

#title3
```

[
   "

Content
",
   "### title1
Content A

Content B
",
   "#title2
",
   "#title3"
]

*/

Input = __ cs:Chapter* { return cs }

Chapter
 = "\n"? "#" cs:ChapterContent* { return "#" + cs.join("") }
  / cs:ChapterContent+ { return cs.join("") }

ChapterContent
  = v:(! "\n#" .) { return v[1] }

NewLine
  = [\r?\n]

__
  = (Space / NewLine)*

_
  = Space*

Space
  = [　 ]
