/**
 * Created by tottokotkd on 17/06/05.
 */

import {PersonRegister} from "./person";
import {SceneRegister, SceneRegisterMode} from "./scene";
import Speaker from "./speaker";

export class Text {

  public static withSpeaker(speaker: Speaker, title: string, values: string[]): Text {
    const text = new Text();
    text.title = title;
    text.speaker = speaker;
    text.values = values;
    return text;
  }

  public static withTitle(title: string, values: string[]): Text {
    const text = new Text();
    text.title = title;
    text.speaker = undefined;
    text.values = values;
    return text;
  }

  public title: string;
  public speaker?: Speaker;
  public values: string[];
}

export default Text;
