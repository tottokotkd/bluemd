/**
 * Created by tottokotkd on 17/06/08.
 */

import {SceneRegister} from "./scene";
import {Text} from "./text";

export class Chapter {
  public title?: string;
  public level?: number;
  public contents: IChapterContent[];
}

export interface IChapterContent {
  scenes: SceneRegister[];
  text?: Text;
}
