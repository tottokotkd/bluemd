var tsconfig = require('./tsconfig.json')
var TsConfigPathsPlugin = require('awesome-typescript-loader').TsConfigPathsPlugin;
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {

  devtool: "source-map",

  resolve: {
    extensions: [".ts", ".js", ".json", ".pegjs"],
    plugins: [
      new TsConfigPathsPlugin({tsconfig}),
    ]
  },

  module: {
    rules: [
      {
        // PEG.js
        test: /\.pegjs$/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }, 'pegjs-loader']
      },
      {
        // typescript
        test: /\.ts$/,
        exclude: /(node_modules|bower_components)/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }, "awesome-typescript-loader"]
      },
      {
        // js source map
        enforce: "pre",
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: "source-map-loader"
      }
    ]
  },
  plugins: [
    new UglifyJSPlugin({sourceMap: true})
  ]
}
