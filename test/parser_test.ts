/**
 * Created by tottokotkd on 17/06/03.
 */

import {expect} from "chai";
import * as Parser from "src";
import {BackgroundRegister} from "../src/background";
import {PersonPosition, PersonRegister} from "../src/person";
import {SceneRegisterMode} from "../src/scene";
import {Speaker} from "../src/speaker";
import {Text} from "../src/text";

describe("Bluemd parser", () => {
  describe("text parser", () => {
    it("should parse a single line to Text object", () => {
      const input = "春菜 さあ！ メガネを！";
      const result = Parser.parseText(input);
      expect(result).to.be.instanceOf(Text);
      expect(result).to.have.property("title", "春菜");
      expect(result).to.have.property("speaker").to.deep.equal(new Speaker("春菜", []));
      expect(result).to.have.property("values").to.deep.equal(["さあ！ メガネを！"]);
    });

    it("should parse lines to Text object", () => {
      const input = "春菜 さあ！ メガネを！\nメガネ！\nめーがーねー！";
      const result = Parser.parseText(input);
      expect(result).to.be.instanceOf(Text);
      expect(result).to.have.keys("title", "speaker", "values");
      expect(result).to.have.property("title", "春菜");
      expect(result).to.have.property("speaker").to.deep.equal(new Speaker("春菜", []));
      expect(result).to.have.property("values").to.deep.equal(["さあ！ メガネを！", "メガネ！", "めーがーねー！"]);
    });

    it("should ignores NewLines after a value", () => {
      const input = "春菜 さあ！ メガネを！";
      const result1 = Parser.parseText(input + "\n");
      expect(result1).to.have.property("values").to.deep.equal(["さあ！ メガネを！"]);
    });

    it("should ignore empty value after header", () => {
      const result1 = Parser.parseText("比奈\nええ…");
      expect(result1).to.have.property("values").to.deep.equal(["ええ…"]);
      const result2 = Parser.parseText("比奈    \nええ…\n眼鏡…");
      expect(result2).to.have.property("values").to.deep.equal(["ええ…", "眼鏡…"]);
    });

    it("should parse optional DESC parameter", () => {
      const result = Parser.parseText("比奈 #困惑 #panic \nええ…");
      expect(result).to.be.instanceOf(Text);
      expect(result).to.have.keys("title", "speaker", "values");
      expect(result).to.have.property("title", "比奈");
      expect(result).to.have.property("speaker").to.deep.equal({name: "比奈", desc: ["困惑", "panic"]});
      expect(result).to.have.property("values").to.deep.equal(["ええ…"]);
    });

    it("should parse TITLE parameter without speaker", () => {
      const result = Parser.parseText(`:"KR  前回のあらすじ"　途方に暮れるヨンジュと練習生仲間たちの前に忽然と現れた猫おじさん。`);
      expect(result).to.be.instanceOf(Text);
      expect(result).to.have.keys("title", "speaker", "values");
      expect(result).to.have.property("title", "KR  前回のあらすじ");
      expect(result).to.have.property("speaker").to.be.undefined;
      expect(result).to.have.property("values").to.deep.equal(["途方に暮れるヨンジュと練習生仲間たちの前に忽然と現れた猫おじさん。"]);
    });

    it("should parse optional TITLE parameter", () => {
      const result = Parser.parseText("比奈 :ｱﾗｷ　忽然と現れた猫おじさん…？");
      expect(result).to.be.instanceOf(Text);
      expect(result).to.have.keys("title", "speaker", "values");
      expect(result).to.have.property("title", "ｱﾗｷ");
      expect(result).to.have.property("speaker").to.be.deep.equal(new Speaker("比奈", []));
      expect(result).to.have.property("values").to.deep.equal(["忽然と現れた猫おじさん…？"]);
    });

  });
  describe("scene parser", () => {
    it("should parse scene selector starting with background", () => {
      const input = "@事務所 +比奈 +春菜";
      const result = Parser.parseScene(input);
      expect(result).to.be.instanceOf(Array);
      expect(result).to.be.length(3);

      const reg1 = result[0];
      expect(reg1).to.be.instanceOf(BackgroundRegister);
      expect(reg1).to.have.property("mode", "add");
      expect(reg1).to.have.property("name", "事務所");

      const reg2 = result[1];
      expect(reg2).to.be.instanceOf(PersonRegister);
      expect(reg2).to.have.property("speaker").to.be.deep.equal(new Speaker("比奈", []));
      expect(reg2).to.have.property("mode", "add");
      expect(reg2).to.have.property("position", "auto");

      const reg3 = result[2];
      expect(reg3).to.be.instanceOf(PersonRegister);
      expect(reg3).to.have.property("speaker").to.be.deep.equal(new Speaker("春菜", []));
      expect(reg3).to.have.property("mode", "add");
      expect(reg3).to.have.property("position", "auto");
    });

    it("should parse scene selector starting with person", () => {
      const input = "+比奈 -春菜";
      const result = Parser.parseScene(input);
      expect(result).to.be.instanceOf(Array);
      expect(result).to.be.length(2);

      const reg1 = result[0];
      expect(reg1).to.be.instanceOf(PersonRegister);
      expect(reg1).to.have.property("speaker").to.be.deep.equal(new Speaker("比奈", []));
      expect(reg1).to.have.property("mode", "add");
      expect(reg1).to.have.property("position", "auto");

      const reg2 = result[1];
      expect(reg2).to.be.instanceOf(PersonRegister);
      expect(reg2).to.have.property("speaker").to.be.deep.equal(new Speaker("春菜", []));
      expect(reg2).to.have.property("mode", "remove");
      expect(reg2).to.have.property("position", "auto");
    });

    it("should parse options for person", () => {
      const input = "+比奈 #照れ #笑顔 -春菜";
      const result = Parser.parseScene(input);
      expect(result).to.be.instanceOf(Array);
      expect(result).to.be.length(2);

      const reg1 = result[0];
      expect(reg1).to.be.instanceOf(PersonRegister);
      expect(reg1).to.have.property("speaker").to.be.deep.equal(new Speaker("比奈", ["照れ", "笑顔"]));
      expect(reg1).to.have.property("mode", "add");
      expect(reg1).to.have.property("position", "auto");

      const reg2 = result[1];
      expect(reg2).to.be.instanceOf(PersonRegister);
      expect(reg2).to.have.property("speaker").to.be.deep.equal(new Speaker("春菜", []));
      expect(reg2).to.have.property("mode", "remove");
      expect(reg2).to.have.property("position", "auto");
    });
  });

  it("should parse bluemd text", () => {
    const input = `
<!--
Bluemd Input text
-->

# ブルーナポレオン

@事務所 +千枝 #笑顔

比奈　テストらしいッス

春菜　#眼鏡　テストですね

千枝　テストです！

瑞樹　よく分からないけど、テストよ

沙理奈　テストにこのテキストいる？

## チャプターテスト

<!--
@ハイエース
-->

-千枝 -春菜 -比奈 -沙理奈
瑞樹　悪意を感じるわね
`;
    const result = Parser.parse(input);
    expect(result).to.be.instanceOf(Array).length(2);

    const chapter1 = result[0];
    expect(chapter1).to.have.property("title", "ブルーナポレオン");
    expect(chapter1).to.have.property("level", 1);
    expect(chapter1).to.have.property("contents").instanceOf(Array).length(6);

    const chapter2 = result[1];
    expect(chapter2).to.have.property("title", "チャプターテスト");
    expect(chapter2).to.have.property("level", 2);
    expect(chapter2).to.have.property("contents").instanceOf(Array).length(3);
  });
});
