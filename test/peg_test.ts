/**
 * Created by tottokotkd on 17/06/05.
 */

import {expect} from "chai";
import * as PEG from "src/peg";

describe("PEG parser", () => {
  describe("text parser", () => {
    it("should parse a single line to Text object", () => {
      const input = "春菜 さあ！ メガネを！";
      const result = PEG.parseText(input);
      expect(result).to.be.instanceOf(Object);
      expect(result).to.have.keys("title", "speaker", "values");
      expect(result).to.have.property("title", "春菜");
      expect(result).to.have.property("speaker").to.deep.equal({name: "春菜", desc: []});
      expect(result).to.have.property("values").to.deep.equal(["さあ！ メガネを！"]);
    });

    it("should parse lines to Text object", () => {
      const input = "春菜 さあ！ メガネを！\nメガネ！\nめーがーねー！";
      const result = PEG.parseText(input);
      expect(result).to.be.instanceOf(Object);
      expect(result).to.have.keys("title", "speaker", "values");
      expect(result).to.have.property("title", "春菜");
      expect(result).to.have.property("speaker").to.deep.equal({name: "春菜", desc: []});
      expect(result).to.have.property("values").to.deep.equal(["さあ！ メガネを！", "メガネ！", "めーがーねー！"]);
    });

    it("should ignores NewLines after a value", () => {
      const input = "春菜 さあ！ メガネを！";
      const result1 = PEG.parseText(input + "\n");
      expect(result1).to.have.property("values").to.deep.equal(["さあ！ メガネを！"]);
    });

    it("should ignore empty value after header", () => {
      const result1 = PEG.parseText("比奈\nええ…");
      expect(result1).to.have.property("values").to.deep.equal(["ええ…"]);
      const result2 = PEG.parseText("比奈    \nええ…\n眼鏡…");
      expect(result2).to.have.property("values").to.deep.equal(["ええ…", "眼鏡…"]);
    });

    it("should parse optional DESC parameter", () => {
      const result = PEG.parseText("比奈 #困惑 #panic \nええ…");
      expect(result).to.be.instanceOf(Object);
      expect(result).to.have.keys("title", "speaker", "values");
      expect(result).to.have.property("title", "比奈");
      expect(result).to.have.property("speaker").to.deep.equal({name: "比奈", desc: ["困惑", "panic"]});
      expect(result).to.have.property("values").to.deep.equal(["ええ…"]);
    });

    it("should parse TITLE parameter without speaker", () => {
      const result = PEG.parseText(`:"KR  前回のあらすじ"　途方に暮れるヨンジュと練習生仲間たちの前に忽然と現れた猫おじさん。`);
      expect(result).to.be.instanceOf(Object);
      expect(result).to.have.keys("title", "speaker", "values");
      expect(result).to.have.property("title", "KR  前回のあらすじ");
      expect(result).to.have.property("speaker").to.be.undefined;
      expect(result).to.have.property("values").to.deep.equal(["途方に暮れるヨンジュと練習生仲間たちの前に忽然と現れた猫おじさん。"]);
    });

    it("should parse optional TITLE parameter", () => {
      const result = PEG.parseText("比奈 :ｱﾗｷ　忽然と現れた猫おじさん…？");
      expect(result).to.be.instanceOf(Object);
      expect(result).to.have.keys("title", "speaker", "values");
      expect(result).to.have.property("title", "ｱﾗｷ");
      expect(result).to.have.property("speaker").to.be.deep.equal({name: "比奈", desc: []});
      expect(result).to.have.property("values").to.deep.equal(["忽然と現れた猫おじさん…？"]);
    });

  });

  describe("scene parser", () => {
    it("should parse scene selector starting with background", () => {
      const input = "@事務所 + => 比奈 +春菜";
      const result = PEG.parseScene(input);
      expect(result).to.be.instanceOf(Array);
      expect(result).to.be.length(3);

      const reg1 = result[0];
      expect(reg1).to.have.keys("mode", "type", "name");
      expect(reg1).to.have.property("mode", "add");
      expect(reg1).to.have.property("type", "background");
      expect(reg1).to.have.property("name", "事務所");

      const reg2 = result[1];
      expect(reg2).to.have.keys("mode", "type", "name", "position", "desc");
      expect(reg2).to.have.property("type", "person");
      expect(reg2).to.have.property("mode", "add");
      expect(reg2).to.have.property("name", "比奈");
      expect(reg2).to.have.property("position", "right");
      expect(reg2).to.have.property("desc").to.be.deep.equal([]);

      const reg3 = result[2];
      expect(reg3).to.have.keys("mode", "type", "name", "position", "desc");
      expect(reg3).to.have.property("type", "person");
      expect(reg3).to.have.property("mode", "add");
      expect(reg3).to.have.property("name", "春菜");
      expect(reg3).to.have.property("position", "auto");
      expect(reg3).to.have.property("desc").to.be.deep.equal([]);
    });

    it("should parse scene selector starting with person", () => {
      const input = "+比奈 -春菜";
      const result = PEG.parseScene(input);
      expect(result).to.be.instanceOf(Array);
      expect(result).to.be.length(2);

      const reg1 = result[0];
      expect(reg1).to.have.keys("mode", "type", "name", "position", "desc");
      expect(reg1).to.have.property("type", "person");
      expect(reg1).to.have.property("mode", "add");
      expect(reg1).to.have.property("name", "比奈");
      expect(reg1).to.have.property("position", "auto");
      expect(reg1).to.have.property("desc").to.be.deep.equal([]);

      const reg2 = result[1];
      expect(reg2).to.have.keys("mode", "type", "name", "position", "desc");
      expect(reg2).to.have.property("type", "person");
      expect(reg2).to.have.property("mode", "remove");
      expect(reg2).to.have.property("name", "春菜");
      expect(reg2).to.have.property("position", "auto");
      expect(reg2).to.have.property("desc").to.be.deep.equal([]);
    });

    it("should parse options for person", () => {
      const input = "+比奈 #照れ #笑顔 -春菜";
      const result = PEG.parseScene(input);
      expect(result).to.be.instanceOf(Array);
      expect(result).to.be.length(2);

      const reg1 = result[0];
      expect(reg1).to.have.keys("mode", "type", "name", "position", "desc");
      expect(reg1).to.have.property("type", "person");
      expect(reg1).to.have.property("mode", "add");
      expect(reg1).to.have.property("name", "比奈");
      expect(reg1).to.have.property("position", "auto");
      expect(reg1).to.have.property("desc").to.be.deep.equal(["照れ", "笑顔"]);

      const reg2 = result[1];
      expect(reg2).to.have.keys("mode", "type", "name", "position", "desc");
      expect(reg2).to.have.property("type", "person");
      expect(reg2).to.have.property("mode", "remove");
      expect(reg2).to.have.property("name", "春菜");
      expect(reg2).to.have.property("position", "auto");
      expect(reg2).to.have.property("desc").to.be.deep.equal([]);
    });
  });

  describe("comment parser", () => {
    it("should ignore comments in single line", () => {
      const input = `春菜　メガネ<!--!!! 漢字にするか考える !!!-->ですよ！ メガネですよ！<!--しつこい-->`;
      const result = PEG.parseComment(input);
      expect(result).to.be.equal("春菜　メガネですよ！ メガネですよ！");
    });

    it("should ignore comments in multiple lines", () => {
      const input = `春菜　メガネ<!--
                    !!! 漢字にするか考える !!!
                    -->ですよ！ メガネですよ！<!--
                    しつこい
                    -->`;
      const result = PEG.parseComment(input);
      expect(result).to.be.equal("春菜　メガネですよ！ メガネですよ！");
    });

    it("should parse scenes in independent comments", () => {
      const input = `春菜　メガネですよ！
<!--
@事務所 + 比奈

このコメントは無視される

:その後…
-->
比奈　…。
`;
      const result = PEG.parseComment(input);
      expect(result).to.be.equal(`春菜　メガネですよ！
@事務所 + 比奈
:その後…
比奈　…。
`);
    });

    it("should parse scenes starting with @", () => {
      const input = "<!-- @事務所 -->";
      const result = PEG.parseComment(input);
      expect(result).to.be.equal("@事務所 ");
    });

    it("should parse scenes starting with +", () => {
      const input = "<!-- +比奈 -->";
      const result = PEG.parseComment(input);
      expect(result).to.be.equal("+比奈 ");
    });

    it("should parse scenes starting with -", () => {
      const input = "<!-- -比奈 -->";
      const result = PEG.parseComment(input);
      expect(result).to.be.equal("-比奈 ");
    });

    it("should parse scenes starting with :", () => {
      const input = "<!-- :猫おじさん -->";
      const result = PEG.parseComment(input);
      expect(result).to.be.equal(":猫おじさん ");
    });

    it("should ignore scenes in normal comments", () => {
      const input = `比奈　うーん…<!--
@事務所 ここには書けない
-->`;
      const result = PEG.parseComment(input);
      expect(result).to.be.equal("比奈　うーん…");
    });

    it("should parse sample text", () => {
      const input = `春菜　メガネ<!--
!!! 漢字にするか考える !!!

-->ですよ！

<!--

ここではシーン設定ができる

@事務所 +比奈 #困惑

タイトルも設定できる

:猫おじさん

-->

比奈　うーん… <!--
@事務所 ここには書けない
-->`;
      const result = PEG.parseComment(input);
      expect(result).to.be.equal(`春菜　メガネですよ！

@事務所 +比奈 #困惑
:猫おじさん

比奈　うーん… `);
    });
  });

  describe("chapters parser", () => {
    it("should split chapters by headers", () => {
      const input = `

Content

### title1
Content A

Content B

#title2

#title3`;
      const result = PEG.parseChapters(input);
      expect(result).to.be.instanceOf(Array);
      expect(result).to.be.deep.equal([
        "Content\n",
        "### title1\nContent A\n\nContent B\n",
        "#title2\n",
        "#title3",
      ]);
    });
  });

  describe("chapter contents parser", () => {
    it("should parse chapter contents with title", () => {
      const input = `###裁判

@事務所 +比奈 +千枝
春菜　この中に…
メガネをかけていない人がいます！

千枝　わー

比奈　わー`;
      const result = PEG.parseChapterContent(input);

      expect(result).to.have.keys("title", "level", "contents");
      expect(result).to.have.property("title", "裁判");
      expect(result).to.have.property("level", 3);
      expect(result).to.have.property("contents").to.be.deep.equal([
        {type: "scene", value: "@事務所 +比奈 +千枝"},
        {type: "text", value: "春菜　この中に…\nメガネをかけていない人がいます！"},
        {type: "text", value: "千枝　わー"},
        {type: "text", value: "比奈　わー"},
      ]);
    });
  });

  it("should parse chapter contents without title", () => {
    const input = `

@事務所 +比奈 +千枝
春菜　この中に…
メガネをかけていない人がいます！

千枝　わー

比奈　わー`;
    const result = PEG.parseChapterContent(input);

    expect(result).to.have.keys("title", "level", "contents");
    expect(result).to.have.property("title", undefined);
    expect(result).to.have.property("level", undefined);
    expect(result).to.have.property("contents").to.be.deep.equal([
      {type: "scene", value: "@事務所 +比奈 +千枝"},
      {type: "text", value: "春菜　この中に…\nメガネをかけていない人がいます！"},
      {type: "text", value: "千枝　わー"},
      {type: "text", value: "比奈　わー"},
    ]);
  });
});
